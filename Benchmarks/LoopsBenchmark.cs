﻿using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using BenchmarkDotNet.Attributes;

namespace Benchmarks;

[MemoryDiagnoser(false)]
public class LoopsBenchmark
{
	[Params(100, 100_000, 1_000_000)]
	public int Size { get; set; }

	private List<int> _list;

	public LoopsBenchmark()
	{
		var r = new Random(30042095);
		_list = Enumerable.Range(1, Size).Select(x => r.Next()).ToList();
	}

	[Benchmark(Baseline = true)]
	public void ForLoop()
	{
		for(int i = 0; i < _list.Count; i++)
		{
			DoSomething(_list[i]);
		}
	}

	[Benchmark]
	public void WhileLoop()
	{
		int i = 0;
		while(i < _list.Count)
		{
			DoSomething(_list[i]);
			i++;
		}
	}

	[Benchmark]
	public void ForEachLoop()
	{
		foreach(int item in _list)
		{
			DoSomething(item);
		}
	}

	[Benchmark]
	public void LinqSum()
	{
		_list.ForEach(DoSomething);
	}

	[Benchmark]
	public void SpanLoop()
	{
		Span<int> span = CollectionsMarshal.AsSpan(_list);
		;
		for(int i = 0; i < span.Length; i++)
		{
			DoSomething(span[i]);
		}
	}

	[Benchmark]
	public void ReadOnlySpanLoop()
	{
		ReadOnlySpan<int> span = CollectionsMarshal.AsSpan(_list);
		for(int i = 0; i < span.Length; i++)
		{
			DoSomething(span[i]);
		}
	}

	[Benchmark]
	public void SpanForeachLoop()
	{
		Span<int> span = CollectionsMarshal.AsSpan(_list);
		;
		foreach(int item in span)
		{
			DoSomething(item);
		}
	}

	[Benchmark]
	public void ReadOnlySpanForeachLoop()
	{
		ReadOnlySpan<int> span = CollectionsMarshal.AsSpan(_list);
		;
		foreach(int item in span)
		{
			DoSomething(item);
		}
	}

	public void UnsafeLoop()
	{
		Span<int> span = CollectionsMarshal.AsSpan(_list);
		ref var searchSpace = ref MemoryMarshal.GetReference(span);
		for(int i = 0; i < span.Length; i++)
		{
			var item = Unsafe.Add(ref searchSpace, i);
			DoSomething(item);
		}
	}


	private void DoSomething(int i) { }
}
