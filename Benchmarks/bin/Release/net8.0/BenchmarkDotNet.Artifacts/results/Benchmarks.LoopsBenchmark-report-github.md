``` ini

BenchmarkDotNet=v0.13.5, OS=Windows 11 (10.0.22000.1696/21H2/SunValley)
12th Gen Intel Core i7-12700K, 1 CPU, 20 logical and 12 physical cores
.NET SDK=8.0.100-preview.4.23260.5
  [Host]     : .NET 8.0.0 (8.0.23.25905), X64 RyuJIT AVX2 [AttachedDebugger]
  DefaultJob : .NET 8.0.0 (8.0.23.25905), X64 RyuJIT AVX2


```
|                  Method |    Size |      Mean |     Error |    StdDev |    Median | Ratio | RatioSD | Allocated | Alloc Ratio |
|------------------------ |-------- |----------:|----------:|----------:|----------:|------:|--------:|----------:|------------:|
|                 **ForLoop** |     **100** | **0.1210 ns** | **0.0107 ns** | **0.0095 ns** | **0.1196 ns** |  **1.00** |    **0.00** |         **-** |          **NA** |
|               WhileLoop |     100 | 0.1212 ns | 0.0062 ns | 0.0058 ns | 0.1219 ns |  1.00 |    0.09 |         - |          NA |
|             ForEachLoop |     100 | 0.1129 ns | 0.0044 ns | 0.0041 ns | 0.1128 ns |  0.94 |    0.09 |         - |          NA |
|                 LinqSum |     100 | 4.3489 ns | 0.0485 ns | 0.0453 ns | 4.3467 ns | 36.15 |    2.60 |      64 B |          NA |
|                SpanLoop |     100 | 0.6399 ns | 0.0304 ns | 0.0284 ns | 0.6346 ns |  5.29 |    0.51 |         - |          NA |
|        ReadOnlySpanLoop |     100 | 0.6056 ns | 0.0214 ns | 0.0189 ns | 0.6095 ns |  5.03 |    0.41 |         - |          NA |
|         SpanForeachLoop |     100 | 0.5644 ns | 0.0231 ns | 0.0216 ns | 0.5605 ns |  4.71 |    0.45 |         - |          NA |
| ReadOnlySpanForeachLoop |     100 | 0.5775 ns | 0.0137 ns | 0.0129 ns | 0.5746 ns |  4.79 |    0.37 |         - |          NA |
|                         |         |           |           |           |           |       |         |           |             |
|                 **ForLoop** |  **100000** | **0.0896 ns** | **0.0159 ns** | **0.0141 ns** | **0.0885 ns** |  **1.00** |    **0.00** |         **-** |          **NA** |
|               WhileLoop |  100000 | 0.1638 ns | 0.0281 ns | 0.0394 ns | 0.1758 ns |  1.66 |    0.46 |         - |          NA |
|             ForEachLoop |  100000 | 0.3223 ns | 0.0130 ns | 0.0122 ns | 0.3202 ns |  3.70 |    0.69 |         - |          NA |
|                 LinqSum |  100000 | 4.8289 ns | 0.2085 ns | 0.6081 ns | 4.5352 ns | 59.47 |   10.60 |      64 B |          NA |
|                SpanLoop |  100000 | 0.9235 ns | 0.0419 ns | 0.0614 ns | 0.9331 ns | 10.63 |    2.11 |         - |          NA |
|        ReadOnlySpanLoop |  100000 | 0.6903 ns | 0.0112 ns | 0.0105 ns | 0.6854 ns |  7.89 |    1.39 |         - |          NA |
|         SpanForeachLoop |  100000 | 0.5648 ns | 0.0367 ns | 0.0572 ns | 0.5760 ns |  6.47 |    1.42 |         - |          NA |
| ReadOnlySpanForeachLoop |  100000 | 0.6834 ns | 0.0379 ns | 0.0839 ns | 0.6726 ns |  7.42 |    1.53 |         - |          NA |
|                         |         |           |           |           |           |       |         |           |             |
|                 **ForLoop** | **1000000** | **0.1227 ns** | **0.0260 ns** | **0.0243 ns** | **0.1392 ns** |  **1.00** |    **0.00** |         **-** |          **NA** |
|               WhileLoop | 1000000 | 0.1211 ns | 0.0106 ns | 0.0094 ns | 0.1203 ns |  1.05 |    0.26 |         - |          NA |
|             ForEachLoop | 1000000 | 0.1036 ns | 0.0080 ns | 0.0071 ns | 0.1047 ns |  0.89 |    0.22 |         - |          NA |
|                 LinqSum | 1000000 | 4.7392 ns | 0.1135 ns | 0.2394 ns | 4.7484 ns | 40.80 |    8.69 |      64 B |          NA |
|                SpanLoop | 1000000 | 0.7078 ns | 0.0381 ns | 0.0298 ns | 0.7060 ns |  6.19 |    1.52 |         - |          NA |
|        ReadOnlySpanLoop | 1000000 | 0.5389 ns | 0.0293 ns | 0.0274 ns | 0.5299 ns |  4.57 |    1.00 |         - |          NA |
|         SpanForeachLoop | 1000000 | 0.6505 ns | 0.0374 ns | 0.0549 ns | 0.6768 ns |  5.23 |    0.93 |         - |          NA |
| ReadOnlySpanForeachLoop | 1000000 | 0.5838 ns | 0.0359 ns | 0.0441 ns | 0.5605 ns |  5.03 |    1.35 |         - |          NA |
